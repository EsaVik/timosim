//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.Serializable;
import java.util.ArrayList;

//SmartPost class. Holds its name and location.
//Latitude and longitude is stored in a GeoPoint
public class SmartPost implements Serializable {

    private GeoPoint gp;
    private String cityName;
    private String dispenserName;
    private String location;
    private String info;

    //Parses all the inputs into easily usable format.
    public SmartPost(float lat, float lon, String code, String city, String address, String availability, String name) {
        gp = new GeoPoint(lat, lon);
        cityName = city;
        dispenserName = name;
        location = address + ", " + code+ " " + city;
        info = name + ", " + availability;
    }
    
    public String getName() {
        return dispenserName;
    }
    
    public String getCity() {
        return cityName;
    }
    
    public String getLocation() {
        return location;
    }
    
    public String getInfo() {
        return info;
    }
    
    public ArrayList getGeoData() {
        return gp.getGeoData();
    }
}
