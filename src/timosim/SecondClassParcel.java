//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

//A type of Parcel
public class SecondClassParcel extends Parcel {
    
    public SecondClassParcel(String nameInput, ShippingObject objectInput, String sourceInput, String destinationInput) {
        super(nameInput, objectInput, sourceInput, destinationInput);
        sizeLimit = 2000;
        price = 90;
        parcelClass = 2;
    }
    
}
