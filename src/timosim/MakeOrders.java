//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.util.Random;
import java.util.TimerTask;
import javafx.application.Platform;

//A task for the timer to make orders for the player.
public class MakeOrders extends TimerTask {

    public static GUIController gc = null;

    @Override
    public void run() {
        Platform.runLater(new Runnable() { //Sends the code to be run in the JavaFX thread. Would get exceptions otherwise.
            @Override
            public void run() {
                if (!((GameState.gs.getBought().size() <= 1) || GameState.gs.getUnlockedObjects().isEmpty())) {
                    Random rand = new Random();
                    int source = rand.nextInt(GameState.gs.getBought().size());
                    int destination = rand.nextInt(GameState.gs.getBought().size());
                    while (destination == source) {
                        destination = rand.nextInt(GameState.gs.getBought().size()); //Makes sure the source and destination aren't the same
                    }
                    int object = rand.nextInt(GameState.gs.getUnlockedObjects().size());
                    String s = GameState.gs.getUnlockedObjects().get(object)
                            + "; " + GameState.gs.getBought().get(source)
                            + " -> " + GameState.gs.getBought().get(destination);
                    if (!GameState.gs.getOrders().contains(s)) {
                        GameState.gs.getOrders().add(s);
                    }
                    if (gc != null) {
                        gc.refreshOrders();
                    }
                }
            }
        });
    }

}
