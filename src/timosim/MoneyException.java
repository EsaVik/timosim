//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

//Used as a thrown exception if the player doesn't have the money for the task
public class MoneyException extends Exception {
}
