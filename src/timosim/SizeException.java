//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

//Used as a thrown exception if the ShippingObject is too big for the Parcel.
public class SizeException extends Exception {
}
