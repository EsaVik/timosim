//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

//A type of ShippingObject
public class Palapeli extends ShippingObject {
    
    public Palapeli() {
        breakable = true;
        broken = false;
        size = 4000;
    }
    
}
