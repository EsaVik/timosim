//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.Serializable;
import java.util.ArrayList;

//Stores a SmartPoint's latitude and longitude
public class GeoPoint implements Serializable {
    
    private ArrayList geoData;
    
    public GeoPoint(float lat, float lon) {
        geoData = new ArrayList();
        geoData.add(lat);
        geoData.add(lon);
    }

    public ArrayList getGeoData() {
        return geoData;
    }
    
}
