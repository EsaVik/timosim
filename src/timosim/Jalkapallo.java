//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

//A type of ShippingObject
public class Jalkapallo extends ShippingObject {
    
    public Jalkapallo() {
        breakable = false;
        broken = false;
        size = 5000;
    }
    
}
