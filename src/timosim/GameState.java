//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.Serializable;
import java.util.ArrayList;

//The class that holds all the gamedata. Serializable so that it can be saved into a file.
//Does calculations like whether you can buy a SmartPost.
//Shares data with the program.
public class GameState implements Serializable {

    public static GameState gs; //So that the program has easy access to the GameState
    private ArrayList<String> log;
    private ArrayList<String> orders;
    private ArrayList<SmartPost> unbought;
    private ArrayList<SmartPost> bought;
    private Storage storage;
    private int money;
    private int moneySpent;
    private int moneyEarned;
    private int ordersDelivered;
    private int parcelsSent;
    private ArrayList<String> unlockedObjects;
    private int smartPostsBought;
    private int smartPostsSold;

    //The constructor initializes the modifiers
    public GameState(ArrayList<SmartPost> smartPosts) {
        unbought = smartPosts;
        bought = new ArrayList();
        log = new ArrayList();
        orders = new ArrayList();
        unlockedObjects = new ArrayList();
        money = 500;
        moneySpent = 0;
        moneyEarned = 0;
        ordersDelivered = 0;
        parcelsSent = 0;
        smartPostsBought = 0;
        smartPostsSold = 0;
        storage = new Storage();
    }

    //Takes in the SmartPost name, returns true is purchase successfull.
    //Throws a MoneyException if there isn't enough money.
    public boolean buy(String name) throws MoneyException {
        if (money >= 100) {
            for (int i = 0; i < unbought.size(); i++) {
                if (unbought.get(i).getName().equals(name)) {
                    bought.add(unbought.get(i));
                    unbought.remove(i);
                    break;
                }
            }
            money = money - 100;
            moneySpent = moneySpent + 100;
            smartPostsBought++;
            return true;
        } else {
            throw new MoneyException();
        }
    }

    //Takes in the SmartPost name. Sells the SmartPost. Adds money.
    public void sell(String name) {
        for (int i = 0; i < bought.size(); i++) {
            if (bought.get(i).getName().equals(name)) {
                unbought.add(bought.get(i));
                bought.remove(i);
                money = money + 75;
                moneyEarned = moneyEarned + 75;
                smartPostsSold++;
                break;
            }
        }
    }

    //Returns a list of unbought SmartPost names for the GUI.
    public ArrayList getUnbought() {
        ArrayList names = new ArrayList();
        for (SmartPost sp : unbought) {
            names.add(sp.getName());
        }
        return names;
    }

    //Returns a list of bought SmartPost names for the GUI.
    public ArrayList getBought() {
        ArrayList names = new ArrayList();
        for (SmartPost sp : bought) {
            names.add(sp.getName());
        }
        return names;
    }

    //Searches for a SmartPost by name. Returns the SmartPost.
    public SmartPost getSmartPost(String name) {
        for (SmartPost sp : unbought) {
            if (sp.getName().equals(name)) {
                return sp;
            }
        }
        for (SmartPost sp : bought) {
            if (sp.getName().equals(name)) {
                return sp;
            }
        }
        return null; //Wouldn't compile without
    }

    //Returns the amount of current money. For GUI and Storage.
    public int getMoney() {
        return money;
    }

    //Used by storage to influence money amount.
    void addMoney(int change) {
        money = money + change;
        moneyEarned = moneyEarned + change;
    }

    public Storage getStorage() {
        return storage;
    }

    public ArrayList getOrders() {
        return orders;
    }

    public ArrayList getLog() {
        return log;
    }

    public void writeLog(String s) {
        log.add(s);
    }

    public ArrayList<String> getUnlockedObjects() {
        return unlockedObjects;
    }

    //Used by storage to influence data from the outside.
    public void parcelSent() {
        parcelsSent++;
    }

    //Used by storage to influence data from the outside.
    public void orderDelivered() {
        ordersDelivered++;
    }
    
    //Used by storage to influence data from the outside.
    public void spendMoney(int i) {
        moneySpent = moneySpent + i;
        money = money - i;
    }

    public int getMoneySpent() {
        return moneySpent;
    }
    
    public int getMoneyEarned() {
        return moneyEarned;
    }
    
    public int getParcelsSent() {
        return parcelsSent;
    }
    
    public int getOrdersDelivered() {
        return ordersDelivered;
    }
    
    public int getSmartPostsSold() {
        return smartPostsSold;
    }
    
    public int getSmartPostsBought() {
        return smartPostsBought;
    }
}
