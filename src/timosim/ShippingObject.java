//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.Serializable;

//ShippingObject abstract class that the objects are based on.
//Has a size and breakable-broken-pair.
public abstract class ShippingObject implements Serializable {

    protected boolean breakable;
    protected boolean broken;
    protected int size;

    public int getSize() {
        return size;
    }

    public boolean isBroken() {
        return broken;
    }

    public void makeBroken() {
        if (breakable) {
            broken = true;
        }
    }

}
