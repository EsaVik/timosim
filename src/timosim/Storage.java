//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.Serializable;
import java.util.ArrayList;

//Storage Class. Holds parcels.
//Creates parcels based on inputs.
//Finds if a sent parcel was in the orders.
//Makes changes to money based on parcel price and whether
//an order was delivered and whether an item was broken if it was an order.
public class Storage implements Serializable {

    private ArrayList<Parcel> parcels;

    public Storage() {
        parcels = new ArrayList();
    }

    //Returns the names of all of the Parcels for GUI usage.
    public ArrayList getParcels() {
        ArrayList names = new ArrayList();
        for (Parcel p : parcels) {
            names.add(p.getName());
        }
        return names;
    }
    
    public Parcel getParcel(int index) {
        return parcels.get(index);
    }

    //Creates a new parcel based on GUI inputs.
    public void addParcel(String nameInput, String classInput, String objectInput, String sourceInput, String destinationInput) {
        ShippingObject object = null;
        if (nameInput.trim().isEmpty()) { //No unnamed parcels
            nameInput = "Paketti";
        }
        if (objectInput.equals("Tiili")) {
            object = new Tiili();
        }
        else if (objectInput.equals("Kukkaruukku")) {
            object = new Kukkaruukku();
        }
        else if (objectInput.equals("Jalkapallo")) {
            object = new Jalkapallo();
        }
        else if (objectInput.equals("Palapeli")) {
            object = new Palapeli();
        }
        int parcelClass = Integer.parseInt(classInput);
        switch (parcelClass) {
            case 1:
                parcels.add(new FirstClassParcel(nameInput, object, sourceInput, destinationInput));
                break;
            case 2:
                parcels.add(new SecondClassParcel(nameInput, object, sourceInput, destinationInput));
                break;
            case 3:
                parcels.add(new ThirdClassParcel(nameInput, object, sourceInput, destinationInput));
                break;
        }
    }

    //Edits a parcel based on GUI inputs.
    public void editParcel(int index, String nameInput, String classInput, String objectInput, String sourceInput, String destinationInput) {
        ShippingObject object = null;
        if (nameInput.equals("")) {
            nameInput = parcels.get(index).getName();
        }
        if (objectInput.equals("Tiili")) {
            object = new Tiili();
        }
        else if (objectInput.equals("Kukkaruukku")) {
            object = new Kukkaruukku();
        }
        else if (objectInput.equals("Jalkapallo")) {
            object = new Jalkapallo();
        }
        else if (objectInput.equals("Palapeli")) {
            object = new Palapeli();
        }
        int parcelClass = Integer.parseInt(classInput);
        switch (parcelClass) {
            case 1:
                parcels.set(index, new FirstClassParcel(nameInput, object, sourceInput, destinationInput));
                break;
            case 2:
                parcels.set(index, new SecondClassParcel(nameInput, object, sourceInput, destinationInput));
                break;
            case 3:
                parcels.set(index, new ThirdClassParcel(nameInput, object, sourceInput, destinationInput));
                break;
        }
    }

    public void removeParcel(int index) {
        parcels.remove(index);
    }

    //Tries to send an order. Throws any exceptions to the GUI controller.
    //Deducts the parcel price from money.
    public void sendParcel(int index, double distance) throws SizeException, DistanceException, MoneyException {
        if (GameState.gs.getMoney() < parcels.get(index).getPrice()) {
            throw new MoneyException();
        }
        GameState.gs.spendMoney(parcels.get(index).send(distance));
        checkOrders(parcels.get(index));
        GameState.gs.writeLog(parcels.get(index).getObject().getClass().getSimpleName()
                + "; " + parcels.get(index).getSource()
                + " -> " + parcels.get(index).getDestination());
        parcels.remove(index);
        GameState.gs.parcelSent();
    }

    //Checks orders for the sent parcel.
    //If the parcel was according to an order, gives the payment.
    //Removes the order.
    private void checkOrders(Parcel parcel) {
        String s = parcel.getObject().getClass().getSimpleName()
                + "; " + parcel.getSource()
                + " -> " + parcel.getDestination();
        if (GameState.gs.getOrders().contains(s)) {
            GameState.gs.addMoney(100);
            if (parcel.getObject().isBroken()) {
                GameState.gs.addMoney(-50);
            }
            GameState.gs.getOrders().remove(s);
            GameState.gs.orderDelivered();
        }
    }

}
