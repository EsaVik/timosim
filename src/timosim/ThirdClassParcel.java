//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

//A type of parcel
public class ThirdClassParcel extends Parcel {
    
    public ThirdClassParcel(String nameInput, ShippingObject objectInput, String sourceInput, String destinationInput) {
        super(nameInput, objectInput, sourceInput, destinationInput);
        sizeLimit = 5000;
        price = 70;
        parcelClass = 3;
    }
    
    @Override
    public int send(double distance) throws SizeException, DistanceException {
        if (shippingObject.getSize() > sizeLimit) {
            throw new SizeException();
        }
        if (shippingObject.getSize() < 4000) {
            shippingObject.makeBroken();
        }
        return price;
    }
    
}
