//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

//Controls the Welcome screen.
public class WelcomeController implements Initializable {

    @FXML
    private TextField nameTextField;
    @FXML
    private Button startButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void onStartButtonPress(ActionEvent event) {
        if (nameTextField.getText().trim().isEmpty()) {
            nameTextField.setText("");
            nameTextField.setPromptText("ANNA NIMESI!"); //Makes sure the player gives a name.
        } else {
            try {
                Main.name = nameTextField.getText().trim();
                DataLoader.loadData(); //Loads a GameState for the game window.
                Stage stage = new Stage();
                Parent root = FXMLLoader.load(getClass().getResource("GUI.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
                Timer orders = new Timer();
                orders.scheduleAtFixedRate(new MakeOrders(), 0, 30000); //Sets a timer to make orders for the player.
                stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent t) {
                        stage.close();
                        orders.cancel(); //Closes the timer thread with the window.
                    }
                });
                ((Stage) startButton.getScene().getWindow()).close();
            } catch (IOException ex) {
                Logger.getLogger(WelcomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Shows the Help Screen
    @FXML
    private void onHelpButtonPress(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("HelpScreen.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
