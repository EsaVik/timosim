//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.Serializable;

//The Parcel abstract class that the three classes are based on.
//Holds an object, has a name and a source and a destination.
//Also has a price dependent on the class.
public abstract class Parcel implements Serializable {

    protected String name;
    protected ShippingObject shippingObject;
    protected String source;
    protected String destination;
    protected int sizeLimit;
    protected int price;
    protected int parcelClass;

    public Parcel(String nameInput, ShippingObject objectInput, String sourceInput, String destinationInput) {
        name = nameInput;
        shippingObject = objectInput;
        source = sourceInput;
        destination = destinationInput;
    }

    public String getName() {
        return name;
    }

    public ShippingObject getObject() {
        return shippingObject;
    }
    
    public int getPrice() {
        return price;
    }

    public int getParcelClass() {
        return parcelClass;
    }
    
    //Returns the price to the sender (Storage)
    //Takes in the distance to check for distanceLimit.
    public int send(double distance) throws SizeException, DistanceException {
        if (shippingObject.getSize() > sizeLimit) {
            throw new SizeException();
        }
        return price;
    }
    
    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }
}
