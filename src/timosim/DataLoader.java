//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

//Static class DataLoader
//Used to load gamedata from a file or the internet and to save the game or delete a save
public class DataLoader {

    private static ArrayList<SmartPost> smartPosts = new ArrayList();

    //Loads gamedata from a file if one exists, else from the internet
    public static void loadData() {
        if (new File(Main.name + ".ser").exists()) {
            ObjectInputStream in = null;
            try {
                in = new ObjectInputStream(new FileInputStream(Main.name + ".ser"));
                GameState.gs = (GameState) in.readObject();
                in.close();
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbFactory.newDocumentBuilder();
                URL url = new URL("http://smartpost.ee/fi_apt.xml");
                Document doc = db.parse(new InputSource(new InputStreamReader(url.openStream())));
                doc.getDocumentElement().normalize();
                parseCurrentData(doc);
            } catch (Exception ex) {
                Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
            }
            GameState.gs = new GameState(smartPosts); //Gives parsed XML-data to GameState as parameter
        }
    }

    //parses XML-data into a format usable by a GameState
    private static void parseCurrentData(Document doc) {
        NodeList nodes = doc.getElementsByTagName("place");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            smartPosts.add(new SmartPost(Float.parseFloat(e.getElementsByTagName("lat").item(0).getTextContent()), //Finds all the data
                    Float.parseFloat(e.getElementsByTagName("lng").item(0).getTextContent()), //for a SmartPost
                    e.getElementsByTagName("code").item(0).getTextContent(),
                    e.getElementsByTagName("city").item(0).getTextContent(),
                    e.getElementsByTagName("address").item(0).getTextContent(),
                    e.getElementsByTagName("availability").item(0).getTextContent(),
                    e.getElementsByTagName("postoffice").item(0).getTextContent()));
        }

    }

    //Saves the current GameState into a .ser file for easy progress saving.
    public static void saveGame() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Main.name + ".ser"));
            out.writeObject(GameState.gs);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Deletes the save if one exists
    public static void deleteSave() {
        File f = new File(Main.name + ".ser"); 
        if (f.exists()) {
            f.delete();
        }
    }

}
