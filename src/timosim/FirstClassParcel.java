//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

//A type of Parcel, with a distance limit
public class FirstClassParcel extends Parcel {

    private int distanceLimit;

    public FirstClassParcel(String nameInput, ShippingObject objectInput, String sourceInput, String destinationInput) {
        super(nameInput, objectInput, sourceInput, destinationInput);
        sizeLimit = 3000;
        distanceLimit = 150;
        price = 40;
        parcelClass = 1;
    }

    //Overrides Parcel send to add distance checking
    @Override
    public int send(double distance) throws SizeException, DistanceException {
        if (shippingObject.getSize() > sizeLimit) {
            throw new SizeException();
        }
        if (distance > distanceLimit) {
            throw new DistanceException();
        }
        shippingObject.makeBroken();
        return price;
    }

}
