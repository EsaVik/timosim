//Author: Esa Vikberg
//Part of the TIMOsim project
//made for Olio-Ohjelmointi course, LUT
//13.12.2015-18.12.2015

package timosim;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class GUIController implements Initializable {

    @FXML
    private WebView mapView;
    @FXML
    private ComboBox<?> buyComboBox;
    @FXML
    private ComboBox<?> sellComboBox;
    @FXML
    private Label moneyLabel;
    @FXML
    private ComboBox<?> sendParcelComboBox;
    @FXML
    private ComboBox<?> createParcelComboBox;
    @FXML
    private TextField nameTextField;
    @FXML
    private ComboBox<?> itemComboBox;
    @FXML
    private ComboBox<?> classComboBox;
    @FXML
    private ComboBox<?> sourceDispenserComboBox;
    @FXML
    private ComboBox<?> destinationDispenserComboBox;
    @FXML
    private ListView<?> ordersListView;
    @FXML
    private TextArea statsTextArea;
    @FXML
    private ListView<?> logListView;

    //Initializes the GUI even from a loaded GameState
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mapView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        moneyLabel.setText(Integer.toString(GameState.gs.getMoney()));
        refreshSmartPostComboBoxes();
        addClasses();
        refreshParcelComboBoxes();
        refreshObjects();
        refreshLog();
        refreshStats();
        MakeOrders.gc = this; //So that the MakeOrders can refresh the orders
    }

    //Tries to buy a new SmartPost. If successful, draws it on the map using javascript in a correct format.
    //If a MoneyException is caught, shows the screen.
    @FXML
    private void buyButtonPressed(ActionEvent event) {
        if (!buyComboBox.getItems().isEmpty()) {
            try {
                if (GameState.gs.buy(buyComboBox.getSelectionModel().getSelectedItem().toString())) {
                    String script = "document.goToLocation('" + GameState.gs.getSmartPost(buyComboBox.getSelectionModel().getSelectedItem().toString()).getLocation() + "', "
                            + "'" + GameState.gs.getSmartPost(buyComboBox.getSelectionModel().getSelectedItem().toString()).getInfo() + "', "
                            + "'blue')";
                    mapView.getEngine().executeScript(script);
                    moneyLabel.setText(Integer.toString(GameState.gs.getMoney()));
                    refreshSmartPostComboBoxes();
                    refreshObjects(); //Buying a new SmartPost can unlock new objects
                    refreshStats(); //A change has been made
                }
            } catch (MoneyException ex) {
                try {
                    Stage stage = new Stage();
                    Parent root = FXMLLoader.load(getClass().getResource("MoneyExceptionScreen.fxml"));
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException ex1) {
                    Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }
    }

    //If there is bought SmartPosts, sells the selected one.
    @FXML
    private void sellButtonPressed(ActionEvent event) {
        if (!sellComboBox.getItems().isEmpty()) {
            mapView.getEngine().executeScript("document.deleteMarker(" + sellComboBox.getSelectionModel().getSelectedIndex() + ")");
            GameState.gs.sell(sellComboBox.getSelectionModel().getSelectedItem().toString());
            refreshSmartPostComboBoxes();
            moneyLabel.setText(Integer.toString(GameState.gs.getMoney()));
            refreshObjects(); //Selling SmartPosts can lock objects
            refreshStats(); //A change has been made
        }
    }

    //Sends a parcel using javascript in a correct format.
    //If an exception is caught, shows the screen.
    @FXML
    private void sendButtonPressed(ActionEvent event) {
        try {
            int index = sendParcelComboBox.getSelectionModel().getSelectedIndex();
            ArrayList pathArray = new ArrayList();
            int parcelClass = GameState.gs.getStorage().getParcel(index).getParcelClass();
            pathArray.addAll(GameState.gs.getSmartPost(GameState.gs.getStorage().getParcel(index).getSource()).getGeoData());
            pathArray.addAll(GameState.gs.getSmartPost(GameState.gs.getStorage().getParcel(index).getDestination()).getGeoData());
            GameState.gs.getStorage().sendParcel(index, (double) mapView.getEngine().executeScript("document.getDistance(" + pathArray + ")"));
            mapView.getEngine().executeScript("document.createPath(" + pathArray + ", 'red', " + parcelClass + ")");
            refreshParcelComboBoxes();
            moneyLabel.setText(Integer.toString(GameState.gs.getMoney()));
            refreshLog();
            refreshOrders();
            refreshStats();
        } catch (SizeException ex) {
            try {
                Stage stage = new Stage();
                Parent root = FXMLLoader.load(getClass().getResource("SizeExceptionScreen.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex1) {
                Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (DistanceException ex) {
            try {
                Stage stage = new Stage();
                Parent root = FXMLLoader.load(getClass().getResource("DistanceExceptionScreen.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex1) {
                Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (MoneyException ex) {
            try {
                Stage stage = new Stage();
                Parent root = FXMLLoader.load(getClass().getResource("MoneyExceptionScreen.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex1) {
                Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    //Deletes drawn paths using javascript.
    @FXML
    private void deletePathsButtonPressed(ActionEvent event) {
        mapView.getEngine().executeScript("document.deletePaths()");
    }

    //Shows shippingclass info screen
    @FXML
    private void classInfoButtonPressed(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("ClassInfo.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    //Saves a new parcel or edits an existing one. depending on which option is chosen.
    @FXML
    private void saveParcelButtonPressed(ActionEvent event) {
        if (!GameState.gs.getBought().isEmpty()) {
            if (createParcelComboBox.getSelectionModel().getSelectedIndex() == 0) {
                GameState.gs.getStorage().addParcel(nameTextField.getText(),
                        classComboBox.getSelectionModel().getSelectedItem().toString(),
                        itemComboBox.getSelectionModel().getSelectedItem().toString(),
                        sourceDispenserComboBox.getSelectionModel().getSelectedItem().toString(),
                        destinationDispenserComboBox.getSelectionModel().getSelectedItem().toString());
            } else {
                GameState.gs.getStorage().editParcel(createParcelComboBox.getSelectionModel().getSelectedIndex() - 1,
                        nameTextField.getText(),
                        classComboBox.getSelectionModel().getSelectedItem().toString(),
                        itemComboBox.getSelectionModel().getSelectedItem().toString(),
                        sourceDispenserComboBox.getSelectionModel().getSelectedItem().toString(),
                        destinationDispenserComboBox.getSelectionModel().getSelectedItem().toString());
            }
            refreshParcelComboBoxes();
        }
    }

    //Deletes the selected parcel.
    @FXML
    private void deleteParcelButtonPressed(ActionEvent event) {
        if (!(createParcelComboBox.getSelectionModel().getSelectedIndex() == 0)) {
            GameState.gs.getStorage().removeParcel(createParcelComboBox.getSelectionModel().getSelectedIndex() - 1);
            refreshParcelComboBoxes();
        }
    }

    //Removes the selected order.
    @FXML
    private void removeOrderButtonPressed(ActionEvent event) {
        if (!ordersListView.getSelectionModel().isEmpty()) {
            GameState.gs.getOrders().remove(ordersListView.getSelectionModel().getSelectedIndex());
            refreshOrders();
        }
    }

    //Saves the game using DataLoader.
    @FXML
    private void saveButtonPressed(ActionEvent event) {
        DataLoader.saveGame();
    }

    //Deletes the save using DataLoader.
    @FXML
    private void deleteSaveButtonPressed(ActionEvent event) {
        DataLoader.deleteSave();
    }

    //Adds all of the shipping classes to the ComboBox.
    private void addClasses() {
        ArrayList classes = new ArrayList();
        classes.add(1);
        classes.add(2);
        classes.add(3);
        classComboBox.getItems().addAll(classes);
        classComboBox.getSelectionModel().selectFirst();
    }

    //Makes sure the info in the SmartPost ComboBoxes is current by reloading it.
    private void refreshSmartPostComboBoxes() {
        buyComboBox.getItems().clear();
        sellComboBox.getItems().clear();
        sourceDispenserComboBox.getItems().clear();
        destinationDispenserComboBox.getItems().clear();
        buyComboBox.getItems().addAll(GameState.gs.getUnbought());
        sellComboBox.getItems().addAll(GameState.gs.getBought());
        sourceDispenserComboBox.getItems().addAll(GameState.gs.getBought());
        destinationDispenserComboBox.getItems().addAll(GameState.gs.getBought());
        buyComboBox.getSelectionModel().selectFirst();
        sellComboBox.getSelectionModel().selectFirst();
        sourceDispenserComboBox.getSelectionModel().selectFirst();
        destinationDispenserComboBox.getSelectionModel().selectFirst();
    }

    //Makes sure the info in the Parcel ComboBoxes is current by reloading it.
    private void refreshParcelComboBoxes() {
        createParcelComboBox.getItems().clear();
        sendParcelComboBox.getItems().clear();
        ArrayList create = new ArrayList();
        ArrayList send = new ArrayList();
        create.add("Luo uusi paketti");
        create.addAll(GameState.gs.getStorage().getParcels());
        send.addAll(GameState.gs.getStorage().getParcels());
        createParcelComboBox.getItems().addAll(create);
        sendParcelComboBox.getItems().addAll(send);
        createParcelComboBox.getSelectionModel().selectFirst();
        sendParcelComboBox.getSelectionModel().selectFirst();
    }

    //Makes sure the info in the ShippingObject ComboBoxes is current by reloading it.
    //Also adds new Objects if the player has progressed far enough.
    private void refreshObjects() {
        ArrayList objects = new ArrayList();
        objects.add("Tiili");
        if (GameState.gs.getBought().size() >= 5) {
            objects.add("Kukkaruukku");
        }
        if (GameState.gs.getBought().size() >= 10) {
            objects.add("Jalkapallo");
        }
        if (GameState.gs.getBought().size() >= 15) {
            objects.add("Palapeli");
        }
        GameState.gs.getUnlockedObjects().clear();
        GameState.gs.getUnlockedObjects().addAll(objects);
        itemComboBox.getItems().clear();
        itemComboBox.getItems().addAll(objects);
        itemComboBox.getSelectionModel().selectFirst();
    }

    //Makes sure the info in the log is current by reloading it.
    private void refreshLog() {
        logListView.getItems().clear();
        logListView.getItems().addAll(GameState.gs.getLog());
    }

    //Makes sure the info in the SmartPost ComboBoxes is current by reloading it.
    //This is public so that the timer that makes the orders can access it.
    public void refreshOrders() {
        ordersListView.getItems().clear();
        ordersListView.getItems().addAll(GameState.gs.getOrders());
    }

    //Refreshes the statsTextArea with current info.
    private void refreshStats() {
        statsTextArea.setText("Rahaa tienattu yhteensä: " + GameState.gs.getMoneyEarned() + System.lineSeparator()
        + "Rahaa käytetty yhteensä: " + GameState.gs.getMoneySpent() + System.lineSeparator()
        + "Paketteja lähetetty yhteensä: " + GameState.gs.getParcelsSent() + System.lineSeparator()
        + "Tilauksia toimitettu yhteensä: " + GameState.gs.getOrdersDelivered() + System.lineSeparator()
        + "SmartPost automaatteja ostettu: " + GameState.gs.getSmartPostsBought() + System.lineSeparator()
        + "SmartPost automaatteja myyty: " + GameState.gs.getSmartPostsSold());
    }
}
